#ifndef REALKNOB_H
#define REALKNOB_H

#include <math.h>

#include <QLabel>
#include <QWidget>
#include <QPixmap>
#include <QPaintEngine>
#include <QRect>
#include <QMouseEvent>
#include <QDebug>

#define badAngle (-99999)

class realKnob : public QLabel
{
    Q_OBJECT

    void getImageCoordinates(int level);
    void loadImage(QString filename);
    void redraw();
    QPixmap fullKnobPixmap;
    QPixmap croppedKnobPixmap;
    QImage *bezel = Q_NULLPTR;
    QPixmap bezelPixmap;
    QPainter *bezPaint = Q_NULLPTR;
    QImage *finalKnobImage = Q_NULLPTR;
    QPainter *finalKnobPainter = Q_NULLPTR;
    QString labelText;
    QFont labelFont;
    int xStartPos=0;
    int yStartPos=0;
    double thetaStartPos=0;
    int imageSizeX = 95;
    int imageSizeY = 95;
    int bezelSizeX = 0;
    int bezelSizeY = 0;
    int bezelDeltaY = 0;
    int bezelDeltaX = 0;
    int releaseAngle = 0;
    double priorAngle = 0;
    double initialAngle = 0;
    double thetaDeltaDeg = 0;
    int currentKnobAngle = 0;

    int lowerLimitDegrees = 0;
    int upperLimitDegrees = 360;

    int userScaleLow = 0;
    int userScaleHigh = 255;
    double userLevelPerDegree = 1;
    int scrollStepDegrees = 10;

    void setup();
    double getAbsAngleFromPosition(double x, double y);
    double getAbsAngleFromPositionOld(double x, double y);

    void computeUserScale();
    int getUserScaleNumber();
    void createBezel();
    void drawBezel();
    void combineImages();
    bool haveDrawnBezel = false;

    // Debug helper function:
    void buildX();
    //QPixmap xPixmap;
    QImage *xImage = Q_NULLPTR;
    //QPainter *xPixPainter = Q_NULLPTR;
    QPainter *xImagePainter = Q_NULLPTR;
    QImage *square = Q_NULLPTR;
    QPainter *squarePainter = Q_NULLPTR;

    // user variables:
    int itemNumber = 0;
    void *itemData = NULL;

public:
    //explicit realKnob(QWidget *parent = nullptr);
    realKnob( QWidget* parent = 0, Qt::WindowFlags f = 0 );
    realKnob( const QString& text, QWidget* parent = 0, Qt::WindowFlags f = 0 );

    void mousePressEvent(QMouseEvent* ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void wheelEvent(QWheelEvent *we);    
    void resizeEvent(QResizeEvent *rev);
    //void paintEvent(QPaintEvent *qp);

    QPixmap getImage();
    int getAbsoluteAngle();
    void setCurrentPositionDegrees(int degrees);
    void setFont(const QFont &f);

    void debugFunction();
    void* getItemData();
    int getItemNumber();
    void setItemData(void* p);
    void setItemNumber(int n);


signals:
    void mousePressed( const QPoint& ); // not used
    void angleChanged(int degrees); // when mouse is dragging
    void angleChangeFinished(int degrees); // on mouse release
    void levelChanged(int level); // same as above but user scale
    void levelChangeFinished(int level); // same as above but user scale
    void angleDelta(int deltaDeg); // degrees knob changed

public slots:
    void setKnobAngleDeg(int level); // emits signal
    void setLevelAbsolute(int level); // no signal emitted
    void setDegreeLimits(int low, int high); // rotational limits
    void setDegreeLimitsDefault(); // rotational limits 45-315 deg
    void setDegreeLimitsContinuous(); // no limits, continuous turning
    void setUserNumericalScaleLevels(int low, int high);
    void setText(QString labelText);

};

#endif // REALKNOB_H
