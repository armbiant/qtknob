This project demonstrates how to build a custom knob, or "dial" using qt widgets and a custom knob image. realKnob inherets from QLabel. To use with qt designer, first add realKnob to your project file, and then simply place a generic widget in the UI and right-click and promote it to a realKnob. 

The sample knob images came from this javascript MIT-Licensed project: 
https://github.com/DusanDimitric/potentiometer

To add this repository as a submodule to your own project, issue the following commands from within your project directory: 

```
git submodule add https://gitlab.com/eliggett/realknob.git/
git submodule init
git submodule update
```


