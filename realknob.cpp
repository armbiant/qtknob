#include "realknob.h"

//realKnob::realKnob(QWidget *parent) : QWidget(parent)
//{
//    loadImage(":/images/res/decapitator_pot_big.png");
//}

realKnob::realKnob( QWidget * parent, Qt::WindowFlags f )
    : QLabel( parent, f ) {
    setup();
}

realKnob::realKnob( const QString& text, QWidget* parent, Qt::WindowFlags f )
    : QLabel( text, parent, f ) {
    labelText = text;
    setup();
}

void realKnob::setup()
{
    loadImage(":/images/res/decapitator_pot_big_transparent.png");
    //loadImage(":/images/res/decapitator_pot_big.png");
    QSize s = size();
    //qDebug() << "CONSTRUCTOR: Initial size: " << s;


    QSize n;
    if(s.width() > s.height())
    {
        n = s.expandedTo(QSize(s.width(), s.width()));
    } else {
        n = s.expandedTo(QSize(s.height(), s.height()));
    }

    //n = s.expandedTo(QSize(bezelSizeX, bezelSizeY));
    n.setHeight(130);
    n.setWidth(130);
    this->resize(n);
    //qDebug() << "CONSTRUCTOR: Final size: " << this->size();
    s = this->size();

    // Now the details:
    bezelSizeX = imageSizeX*1.25;
    bezelSizeY = imageSizeY*1.25;

    bezelDeltaX = bezelSizeX - imageSizeX;
    bezelDeltaY = bezelSizeY - imageSizeY;
    labelFont = QFont("monospace", 10);
    bezel = new QImage(QSize(bezelSizeX, bezelSizeY), QImage::Format_ARGB32_Premultiplied);
    bezel->fill(Qt::transparent);
    bezPaint = new QPainter(bezel);


    finalKnobImage = new QImage(s, QImage::Format_ARGB32_Premultiplied);
    finalKnobImage->fill(Qt::transparent);
    finalKnobPainter = new QPainter(finalKnobImage);

    //qDebug() << "CONSTRUCTOR: finalKnobImage: " << finalKnobImage->size() << ", bezel: " << bezel->size();

    buildX();

    lowerLimitDegrees = 45;
    upperLimitDegrees = 315;
    setDegreeLimitsDefault();
//    computeUserScale();
//    createBezel();
//    combineImages();
//    redraw();
}

void realKnob::resizeEvent(QResizeEvent *rev)
{
    (void)rev;
    qDebug() << "Resize event triggered. Number: " << this->itemNumber << ", Size: " << this->size();
    redraw();
}

void realKnob::buildX()
{
    // This is a debug function to build a simple X, which can be placed to help locate
    // coordinates and issues.
    xImage = new QImage(20,20,QImage::Format_ARGB32_Premultiplied);
    xImagePainter = new QPainter(xImage);
    xImage->fill(Qt::transparent);
    xImagePainter->setPen(QPen(Qt::red));
    xImagePainter->drawLine(0,0, 20,20);
    xImagePainter->setPen(QPen(Qt::yellow));
    xImagePainter->drawLine(0,20,20,0);

    square = new QImage(finalKnobImage->height(), finalKnobImage->width(), QImage::Format_ARGB32_Premultiplied);
    squarePainter = new QPainter(square);
    square->fill(Qt::transparent);
    squarePainter->setPen(QPen(Qt::green, 3, Qt::SolidLine, Qt::RoundCap));
    squarePainter->drawRect(0,0,square->height(), square->width());
}

void realKnob::loadImage(QString filename)
{
    fullKnobPixmap.load(filename);
    int x = 0;
    int y = 0;
    int width = fullKnobPixmap.width();
    int height = 95; // this is the height of each "step" of the knob.

    QRect rect(x, y, width, height);
    croppedKnobPixmap = fullKnobPixmap.copy(rect);
}

double realKnob::getAbsAngleFromPositionOld(double x, double y)
{
    //-----------------------------|
    //  1,1      (180)       95,1  |
    //                             |
    //                             |
    // (90)        @         (270) |
    //                             |
    //                             |
    //                             |
    //  1,95                 95,95 |
    //-----------------------------|
    //-------------(0)-------------|

    double yScaled = ((imageSizeY/2.0) - y );
    double xScaled = (x - (imageSizeX/2.0));

    double thetaDeg = 0;

    //if( (x < imageSizeX) && (y < imageSizeY) && (x>0) && (y>0) )
    {
        thetaDeg = (yScaled||xScaled) ? atan2(xScaled,yScaled) * 180 / 3.14159 : 0;
        thetaDeg += 180;
        //qDebug() << "X: " << x << ", Y: " << y << ", absolute angle: " << thetaDeg << " degrees";
        return thetaDeg;
    }
    //return badAngle;
}

double realKnob::getAbsAngleFromPosition(double x, double y)
{
    //-----------------------------|
    //  1,1      (180)       95,1  |
    //                             |
    //                             |
    // (90)        @         (270) |
    //                             |
    //                             |
    //                             |
    //  1,95                 95,95 |
    //-----------------------------|
    //-------------(0)-------------|

    // This widget always scales as a square,
    // However, it may have an oblong dimension.
    // Thus, we grab the smaller of the two.

    double offset;
    QSize s = size();
    if(s.width() > s.height())
    {
        offset = s.height();
    } else {
        offset = s.width();
    }


    double yScaled = ((offset/2.0) - y );
    double xScaled = (x - (offset/2.0));

    double thetaDeg = 0;


    thetaDeg = (yScaled||xScaled) ? atan2(xScaled,yScaled) * 180 / 3.14159 : 0;
    thetaDeg += 180;
    //qDebug() << "X: " << x << ", Y: " << y << ", absolute angle: " << thetaDeg << " degrees";
    return thetaDeg;

}

void realKnob::wheelEvent(QWheelEvent *we)
{
    // position of mouse during scroll event:
    //int x = we->x();
    //int y= we->y();

    // Change in "points" for scrolling. May be horizontal or vertical scroll:
    // steps seem to be +/- 120 every time
    int delta = we->delta();
    int scrolls = delta / 120;
    int steps = scrolls * scrollStepDegrees;

    // 2-dim change in points, shows horizontal and vertical scroll
    //QPoint deltaang = we->angleDelta();

    // ALT key modifier swaps horizontal and vertical scroll.

    //qDebug() << "Scroll, x: " << x << "y: " << y << "delta" << delta << "qpoint" << deltaang;
    //qDebug() << "Scrolls: " << scrolls << "steps" << steps << "Current angle: " << currentKnobAngle << "currentknobangle + steps: " << currentKnobAngle + steps;
    releaseAngle = currentKnobAngle;

    setKnobAngleDeg(steps);
    combineImages();
    redraw();
}

void realKnob::mousePressEvent(QMouseEvent *ev)
{
    int x = ev->x();
    int y = ev->y();
    int a = (int)getAbsAngleFromPosition(x,y);

    if(a != badAngle)
        thetaStartPos = a;
    //qDebug() << "Mouse press at position: x:" << x << ", y:" << y << "my size: " << this->size();
}

void realKnob::mouseReleaseEvent(QMouseEvent *ev)
{
    (void)ev;
    releaseAngle = currentKnobAngle;
    emit angleChangeFinished(releaseAngle);
    emit levelChangeFinished( getUserScaleNumber() );
}

void realKnob::mouseMoveEvent(QMouseEvent *ev)
{
    // We only get here if the mouse has already clicked and it's being held down.
    int x = ev->x();
    int y = ev->y();
    double a = getAbsAngleFromPosition(x,y);

    if(a != badAngle)
    {
        thetaDeltaDeg = a - thetaStartPos; // from start of mouse press to current position

        setKnobAngleDeg(thetaDeltaDeg);
        combineImages();
        redraw();
    }
}

void realKnob::redraw()
{
    //this->setPixmap(croppedKnobPixmap);
    //qDebug() << "image size to draw: " << finalKnobImage->size();
    //qDebug() << "pixmap size: " << QPixmap::fromImage(*finalKnobImage).size();

    // You can scale the image for disasterous results:
    this->setPixmap(QPixmap::fromImage(finalKnobImage->scaled(this->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation)));
    //this->setPixmap(QPixmap::fromImage(*finalKnobImage));

}

void realKnob::computeUserScale()
{
    // compute or re-compute the user scale
    // from userMin to userMax over the
    // degree range of permitted rotation
    int userSpan = userScaleHigh - userScaleLow;
    int degSpan = upperLimitDegrees - lowerLimitDegrees;
    if( userSpan && degSpan )
    {
        userLevelPerDegree = userSpan / (double)(degSpan);
    }
}

int realKnob::getUserScaleNumber()
{
    return ((currentKnobAngle-lowerLimitDegrees)*userLevelPerDegree) + userScaleLow;
}

void realKnob::createBezel()
{
    // The bezel is approzimately 25% larger than the knob.

    int bezCenterX = (bezel->width() / 2);
    int bezCenterY = (bezel->height() / 2);

    int centerX = (imageSizeX/2.0) + (bezelDeltaX/2.0);
    int centerY = (imageSizeY/2.0) + (bezelDeltaY/2.0);

    QPointF ctr(centerX, centerY);
    QPointF edge(imageSizeX, imageSizeY);
    //bezPaint->fillRect(QRectF(0,0,centerX,centerY), QBrush(QColor(Qt::red)));
    bezel->fill(QColor(Qt::transparent));
    bezelPixmap.fill(QColor(Qt::transparent));

    //qDebug() << "Does the bezel have an alpha channel? " << bezel->hasAlphaChannel();
    bezPaint->setRenderHints( QPainter::HighQualityAntialiasing );
    bezPaint->setPen(QPen(Qt::white, 1.5, Qt::SolidLine, Qt::RoundCap));
    int xOuter=0;
    int yOuter=0;
    int xInner = 0;
    int yInner = 0;
    int radiusOuter = bezCenterX * 0.9;
    int radiusInner = 5;

    //bezPaint->drawPixmap(bezelDeltaX/2.0,bezelDeltaY/2.0,imageSizeX, imageSizeY, cropped);
    //for(int theta=45; theta < 360-44; theta = theta + 15)
    //qDebug() << "Drawing bezel for lower:" << lowerLimitDegrees << ", higher: " << upperLimitDegrees;

    for(int theta=lowerLimitDegrees; theta < upperLimitDegrees+1; theta = theta + 15)
    {
        //qDebug() << "Drawing bezel line at theta = " << theta << "degrees";
        xOuter = (radiusOuter * sin(theta*M_PI/180)) + bezCenterX;
        yOuter = (radiusOuter * cos(theta*M_PI/180)) + bezCenterY;
        edge.setX(xOuter);
        edge.setY(yOuter);

        xInner = (radiusInner * sin(theta*M_PI/180)) + bezCenterX;
        yInner = (radiusInner * cos(theta*M_PI/180)) + bezCenterY;
        ctr.setX(xInner);
        ctr.setY(yInner);

        bezPaint->drawLine(ctr, edge);
    }

    // Draw an X in the center for alignment
    //bezPaint->drawImage(bezCenterX-10, bezCenterY-10, *xImage);


    //bezPaint->drawPixmap(bezelDeltaX/2.0,bezelDeltaY/2.0,imageSizeX, imageSizeY, croppedKnobPixmap);

    if(!labelText.isEmpty())
    {
        //qDebug() << "adding text: " << labelText;
        bezPaint->setFont(labelFont);
        QFontMetrics fm(labelFont);
        int textWidth=fm.horizontalAdvance(labelText);
        //int textHeight=fm.height();
        bezPaint->drawText(centerX-(textWidth/2),(centerY*2)-2, labelText);
    }

    bezelPixmap = QPixmap::fromImage(*bezel);
    //this->setPixmap( bezelPixmap );
    //this->update();
    haveDrawnBezel = true;
}

void realKnob::combineImages()
{
    int finalW = finalKnobImage->width();
    //int finalH = finalKnobImage->height();
    //int cropKnY = croppedKnobPixmap.height();
    int cropKnX = croppedKnobPixmap.width();

    // Uncomment this line if you wish to verify that the image is indeed cleared:
    //finalKnobPainter->fillRect(QRectF(0,0,finalW, finalH), QBrush(QColor(Qt::red)));

    finalKnobImage->fill(QColor(Qt::transparent)); // clear the image

    if(haveDrawnBezel)
    {
        // Draw the bezel:
        //finalKnobImage = QPixmap::fromImage(*bezel);
        finalKnobPainter->drawImage( (finalW-bezelSizeX) / 2, 0, *bezel); // at half delta x and y
        //finalKnobPainter->drawPixmap(bezelDeltaX/2.0,bezelDeltaY/2.0,imageSizeX, imageSizeY, bezelPixmap);
        //qDebug() << "Combining image with bezel";
    } else {
        //finalKnobImage = croppedKnobPixmap.toImage();
        //qDebug() << "Not combining image with bezel.";
    }

    // Let's try an experiment:
    // Top-left corner:
    //finalKnobPainter->drawImage(0,0,*xImage);

    // draw X based on image size:
    //finalKnobPainter->drawImage(finalKnobImage->width()-20, finalKnobImage->height()-20, *xImage);

    // Draw a square to show the edge:
    //finalKnobPainter->drawImage(0,0,*square);

    // Draw X based on the widget size:
    //finalKnobPainter->drawImage(this->size().width(),this->size().height(),*xImage);
    //finalKnobPainter->drawImage(this->size().width()/2,this->size().height()/2,*xImage);
    //qDebug() << "Combine images: The size of the widget is: " << this->size();
    // Tack on the cropped knob last:
    // The +1 seems to look better. It is an odd number so it does not divide nicely anyway.
    finalKnobPainter->drawPixmap(  QPointF(0+(finalW - cropKnX) / 2.0, (bezelSizeY-imageSizeY)/2.0) , croppedKnobPixmap);

    //finalKnobPainter->drawPixmap(1+(finalW - cropKnX) / 2.0,(bezelSizeY-imageSizeY)/2.0 ,imageSizeX, imageSizeY, croppedKnobPixmap);
    //finalKnobPainter->drawImage((finalKnobImage->width()/2)-10, (finalKnobImage->height()/2)-10, *xImage);

    //this->setPixmap( finalKnobPixmap );
}

void realKnob::drawBezel()
{
    //QPaintEngine *p = this->paintEngine();
    //p->drawLines();
}

int realKnob::getAbsoluteAngle()
{
    // Returns a number 0 to 360 degrees.
    return currentKnobAngle;
}

void realKnob::setLevelAbsolute(int level)
{
    int x = 0;
    int y = level*95;
    int width = 95;
    int height = 95;
    QRect rect(x, y, width, height);
    croppedKnobPixmap = fullKnobPixmap.copy(rect);
}

void realKnob::setKnobAngleDeg(int angleDegDelta)
{
    emit angleDelta(angleDegDelta);
    int angleDeg = (angleDegDelta + releaseAngle) % 360;

    if(angleDeg < 0)
        angleDeg = 360 + angleDeg;

    if (angleDeg < lowerLimitDegrees)
        angleDeg = lowerLimitDegrees;


    if(angleDeg > upperLimitDegrees)
        angleDeg = upperLimitDegrees;

    // 0 to 130 --> 0-360
    int level = ((int)(angleDeg * 130.0 / 360.0));
    int x = 0;
    int y = level*95;
    int width = 95;
    int height = 95;

    if( (level < 130) && (level >= 0) )
    {
        //qDebug() << "Image crop Level: " << level << ", angle: " << angleDeg << " degrees, y: " << y;
        QRect rect(x, y, width, height);
        croppedKnobPixmap = fullKnobPixmap.copy(rect);
        currentKnobAngle = angleDeg;
        emit angleChanged(currentKnobAngle);
        emit levelChanged( getUserScaleNumber() );
    } else {
        qWarning() << "WARNING: Image cannot be cropped to level " << level << " for angle of " << angleDeg << " degrees";
    }
}

QPixmap realKnob::getImage()
{
    // Debug function, not needed
    return croppedKnobPixmap;
}

void realKnob::setCurrentPositionDegrees(int degrees)
{
    // Use for initial position
    currentKnobAngle = 0;
    releaseAngle = 0;
    setKnobAngleDeg(degrees);
    currentKnobAngle = degrees;
    releaseAngle = degrees;
    redraw();
}

void realKnob::setDegreeLimitsContinuous()
{
    setDegreeLimits(0,360);
}

void realKnob::setDegreeLimitsDefault()
{
    setDegreeLimits(45,315);
}

void realKnob::setDegreeLimits(int low, int high)
{
    if( (low >= 0) && (high <=360))
    {
        if( (currentKnobAngle < low) || (currentKnobAngle > high) )
        {
            // set the knob to something within the limits first
            // otherwise, can't move the knob
            // Set the knob to whichever limit is closest to the current position
            int distLow = (currentKnobAngle - low);
            int distHigh = (high - currentKnobAngle);
            if(distLow < distHigh)
            {
                setCurrentPositionDegrees(low);
            } else {
                setCurrentPositionDegrees(high);
            }
        }
        lowerLimitDegrees = low;
        upperLimitDegrees = high;
        computeUserScale();
        createBezel();
        combineImages();
        redraw();
    }
}

void realKnob::setUserNumericalScaleLevels(int low, int high)
{
    if( (high > low) && (low >= 0) && (high > 0) )
    {
        userScaleLow = low;
        userScaleHigh = high;
        computeUserScale();
    }
}

void realKnob::setText(QString labelText)
{
    this->labelText = labelText;
    createBezel();
    combineImages();
    redraw();
}

void realKnob::setFont(const QFont &f)
{
    this->labelFont = f;
    createBezel();
    combineImages();
    redraw();
}

// Helper functions to hold useful external data:

void* realKnob::getItemData()
{
    return this->itemData;
}

int realKnob::getItemNumber()
{
    return this->itemNumber;
}

void realKnob::setItemData(void *p)
{
    this->itemData = p;
}

void realKnob::setItemNumber(int n)
{
    this->itemNumber = n;
    //qDebug() << "set item number: " << n;
}

void realKnob::debugFunction()
{
    qDebug() << "Reached debug function inside realKnob.";
    combineImages();
}
